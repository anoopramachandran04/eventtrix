import React, { useState,useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Home from "./Components/Pages/Home/index";


import LoginPage from "./Components/Pages/Auth/login";
import AuthLayout from "./Components/Layouts/AuthLayout";
import { GlobalState } from "./context-api";

const Routes = () => {
  const { Authenticated } = useContext(GlobalState);
  
  return (
    <Router>
      {!Authenticated ? (
        <AuthLayout>
          <Route  path="/*" component={LoginPage} />
        </AuthLayout>
      ) : (
       
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/dashboard" component={Home} />
            <Redirect to="/dashboard" />
          </Switch>
       
      )}
    </Router>
  );
};

export default Routes;
