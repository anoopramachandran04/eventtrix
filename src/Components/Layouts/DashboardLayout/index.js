import React, { useState, useContext } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { Button, Layout, Menu, Dropdown } from "antd";
import { Link, NavLink } from "react-router-dom";
import { GlobalState } from "./../../../context-api";
import Cookies from "universal-cookie";
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const DashboardLayout = (props) => {
  const {
    setAuthenticated,
    LoggedUserEmail,
    setLoggedUserEmail,
    setTotalLoading,
  } = useContext(GlobalState);
  const [Collapsed, setCollapsed] = useState(false);
  const cookies = new Cookies();

  const toggle = () => {
    setCollapsed(!Collapsed);
  };


  const ActionMenu = (
    <Menu>
      <Menu.Item
        key="1"
        onClick={() => {
          setTotalLoading(true);
          cookies.remove("session");
            localStorage.removeItem("LoggedUserEmail");
          setTimeout(() => {
            setAuthenticated(false);
            setLoggedUserEmail("");
            setTotalLoading(false);
          }, 1000);
        }}
      >
        Logout
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="dashboard-layout">
      <Layout>
        <Sider trigger={null} collapsible collapsed={Collapsed}>
          <Link to="/">
            <div className="logo">
              <img
               
                className="max-width-100"
              />
            </div>
          </Link>
          
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background p-0">
            {React.createElement(
              Collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: toggle,
              }
            )}
            <div className="header-right-content">
              <Dropdown.Button overlay={ActionMenu}>
                {LoggedUserEmail ? LoggedUserEmail : "Admin"}
              </Dropdown.Button>
            </div>
          </Header>
          <Content className="site-layout-background content">
            {props.children}
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};
export default DashboardLayout;
