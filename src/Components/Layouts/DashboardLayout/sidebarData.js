
import DashboardIcon from "./../../../Images/sidebar-icons/dashboard.svg"
import HospitalIcon from "./../../../Images/sidebar-icons/hospital.svg"
import DoctorIcon from "./../../../Images/sidebar-icons/doctor.svg"
import LabIcon from "./../../../Images/sidebar-icons/lab.svg"
import PatientIcon from "./../../../Images/sidebar-icons/patient.svg"
import SpecialityIcon from "./../../../Images/sidebar-icons/speciality.svg"
import PractitionerTypeIcon from "./../../../Images/sidebar-icons/practitionertype.svg"
import AppointmentTypeIcon from "./../../../Images/sidebar-icons/appointmenttype.svg"
import ServiceIcon from "./../../../Images/sidebar-icons/service.svg"
import StateIcon from "./../../../Images/sidebar-icons/state.svg"
import DistrictIcon from "./../../../Images/sidebar-icons/district.svg"
import UserTypeIcon from "./../../../Images/sidebar-icons/usertype.svg"
import LabTestIcon from "./../../../Images/sidebar-icons/labtest.svg"
import LanguageSpokenIcon from "./../../../Images/sidebar-icons/languagespoken.svg"
import RewardTypeIcon from "./../../../Images/sidebar-icons/rewardtype.svg"
import AppointmentIcon from "./../../../Images/sidebar-icons/appointment.svg"
import LabAppointmentIcon from "./../../../Images/sidebar-icons/labappointment.svg"
import EPrescriptionIcon from "./../../../Images/sidebar-icons/eprescription.svg"
import ReviewIcon from "./../../../Images/sidebar-icons/review.svg"
import ProcedurePaymentIcon from "./../../../Images/sidebar-icons/procedurepayment.svg"
import AppointmentPaymentIcon from "./../../../Images/sidebar-icons/appointmentpayment.svg"
import PackageRefundIcon from "./../../../Images/sidebar-icons/packagerefund.svg"
import RewardRequestIcon from "./../../../Images/sidebar-icons/rewardrequest.svg"

export const mainItems = [
  {  title: "Dashboard", icon: <img src={DashboardIcon} className="sidebar-icon" /> ,link:"/dashboard"}
];

export const accountItems = [
  {  title: "Hospitals", icon: <img src={HospitalIcon} className="sidebar-icon" /> ,link:"/hospitals"},
  {  title: "Practitioners", icon:<img src={DoctorIcon} className="sidebar-icon" />,link:"/practitioners"},
  {  title: "Labs", icon:<img src={LabIcon} className="sidebar-icon" />,link:"/labs"},
  {  title: "Patients", icon:<img src={PatientIcon} className="sidebar-icon" />,link:"/patients"}
];

export const generalItems = [
  {  title: "Procedures", icon:  <img src={LabTestIcon} className="sidebar-icon" /> ,link:"/procedures"},
  {  title: "Specialities", icon: <img src={SpecialityIcon} className="sidebar-icon" />,link:"/specialities" },
  { title: "Practitioner Types", icon: <img src={PractitionerTypeIcon} className="sidebar-icon" />,link:"/practitioner-types" },
  { title: "Appointment Types", icon: <img src={AppointmentTypeIcon} className="sidebar-icon" />,link:"/appointment-types" },
  { title: "Services", icon: <img src={ServiceIcon} className="sidebar-icon" />,link:"/services" },
  {  title: "EPrescription", icon:<img src={EPrescriptionIcon} className="sidebar-icon" />,link:"/ePrescription"},
  { title: "States", icon: <img src={StateIcon} className="sidebar-icon" />,link:"/states" },
  { title: "Districts", icon: <img src={DistrictIcon} className="sidebar-icon" />,link:"/districts" },
  { title: "User Types", icon: <img src={UserTypeIcon} className="sidebar-icon" />,link:"/user-types" },
  { title: "Lab Tests", icon: <img src={LabTestIcon} className="sidebar-icon" />,link:"/lab-tests" },
  { title: "Languages Spoken", icon: <img src={LanguageSpokenIcon} className="sidebar-icon" />,link:"/languages-spoken" },
  { title: "Reward Types", icon: <img src={RewardTypeIcon} className="sidebar-icon" />,link:"/reward-type" }
];
export const reportsItems = [
  {  title: "Appointments", icon: <img src={AppointmentIcon} className="sidebar-icon" /> ,link:"/appointments"},
  {  title: "Lab Appointments", icon: <img src={LabAppointmentIcon} className="sidebar-icon" />,link:"/lab-appointments"},
  {  title: "Reviews", icon: <img src={ReviewIcon} className="sidebar-icon" />,link:"/reviews"},
  {  title: "Procedure Payments", icon:<img src={ProcedurePaymentIcon} className="sidebar-icon" /> ,link:"/procedure-payments"},
  {  title: "Appointment Payments", icon:<img src={AppointmentPaymentIcon} className="sidebar-icon" /> ,link:"/appointment-payments"},
  {  title: "Package Refund", icon: <img src={PackageRefundIcon} className="sidebar-icon" /> ,link:"/package-refund"},
  {  title: "Reward Requests", icon:<img src={RewardRequestIcon} className="sidebar-icon" /> ,link:"/reward-requests"}

];

