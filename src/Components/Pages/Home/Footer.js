import React, { useEffect, useState } from "react";
const Footer = () => {
  
  useEffect(() => {
    
  }, []);
  return (
    <div>
   <footer id="footer">
    <div className="footer-top">
      <div className="container">
        <div className="row">

          <div className="col-lg-3 col-md-6 footer-info">
            <img src="assets/img/logo.png" alt="TheEvenet" />
            <p>In alias aperiam. Placeat tempore facere. Officiis voluptate ipsam vel eveniet est dolor et totam porro. Perspiciatis ad omnis fugit molestiae recusandae possimus. Aut consectetur id quis. In inventore consequatur ad voluptate cupiditate debitis accusamus repellat cumque.</p>
          </div>

          <div className="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i className="fa fa-angle-right"></i> <a href="#">Home</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">About us</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Services</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div className="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i className="fa fa-angle-right"></i> <a href="#">Home</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">About us</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Services</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
              <li><i className="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div className="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              A108 Adam Street <br />
              New York, NY 535022<br />
              United States <br />
              Phone: +1 5589 55488 55<br />
              Email: info@example.com<br />
            </p>

           

          </div>

        </div>
      </div>
    </div>

    <div className="container">
      <div className="copyright">
        &copy; Copyright <strong>TheEvent</strong>. All Rights Reserved
      </div>
      <div className="credits">
      
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=TheEvent
       
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer>
  <a href="#" className="back-to-top"><i className="fa fa-angle-up"></i></a>
<script src="assets/lib/jquery/jquery.min.js"></script>
<script src="assets/lib/jquery/jquery-migrate.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/lib/easing/easing.min.js"></script>
<script src="assets/lib/superfish/hoverIntent.js"></script>
<script src="assets/lib/superfish/superfish.min.js"></script>
<script src="assets/lib/wow/wow.min.js"></script>
<script src="assets/lib/venobox/venobox.min.js"></script>
<script src="assets/lib/owlcarousel/owl.carousel.min.js"></script>

<script src="assets/contactform/contactform.js"></script>

<script src="assets/js/main.js"></script>

        </div>
  );

};
export default Footer;
