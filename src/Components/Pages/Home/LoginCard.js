import React from "react";

const LoginCard = (props) => {
  return (
    <div className="modal-dialog login animated">
    <div className="modal-content">
       <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 className="modal-title">Login with</h4>
          </div>
          <div className="modal-body">
            
              <div className="box">
                  <div className="content registerBox" style="display:none;">
                   <div className="form">
                      <form method="" html="{:multipart=>true}" data-remote="true" action="" accept-charset="UTF-8">
                      <input id="email" className="form-control" type="text" placeholder="Email" name="email" />
                      <input id="password" className="form-control" type="password" placeholder="Password" name="password" />
                      <input id="password_confirmation" className="form-control" type="password" placeholder="Repeat Password" name="password_confirmation" />
                      <input className="btn btn-default btn-register" type="button" value="Create account" name="commit" />
                      </form>
                      </div>
                  </div>
              </div>
          </div>
          <div className="modal-footer">
              <div className="forgot login-footer">
                  <span>Looking to
                       <a href="javascript: showRegisterForm();">create an account</a>
                  ?</span>
              </div>
              <div className="forgot register-footer" style="display:none">
                   <span>Already have an account?</span>
                   <a href="javascript: showLoginForm();">Login</a>
              </div>
          </div>
    </div>
</div>
  );
};
export default LoginCard;
