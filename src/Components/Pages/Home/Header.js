import React, { useEffect, useState } from "react";
const Header = () => {
  const [showDetailsModal, setshowDetailsModal] = useState(false);
  
  useEffect(() => {
    
  }, []);
  return (
    <div>
   <header id="header">
    <div className="container">

      <div id="logo" className="pull-left">
       
        <a href="#intro" className="scrollto"><img src="assets/img/logo.png" alt="" title="" /></a>
      </div>

      <nav id="nav-menu-container">
        <ul className="nav-menu">
          <li><a href="#intro">Home</a></li>
          <li><a href="#schedule">Event Lineups</a></li>
          <li><a href="#buy-tickets">Buy Tickets</a></li>
         
        </ul>
      </nav>
     
    </div>
  </header>

        </div>
  );

};
export default Header;
