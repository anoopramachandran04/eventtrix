import React, { useEffect, useState } from "react";
import Header from "./Header";
import Footer from "./Footer"
const Home = () => {
  
  useEffect(() => {
    
  }, []);
  return (
    <div>
  <Header />

  <section id="intro">
    <div className="intro-container">
    <h1 className="mb-4 pb-0">
      The Annual
      <span>Marketing</span> 
      Conference
      </h1>
      <p className="mb-4 pb-0">10-12 December, Downtown Conference Center, New York</p>
     
     
    </div>
  </section>

  <main id="main">

  <section id="about">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <h2>About The Event</h2>
            <p>Sed nam ut dolor qui repellendus iusto odit. Possimus inventore eveniet accusamus error amet eius aut
              accusantium et. Non odit consequatur repudiandae sequi ea odio molestiae. Enim possimus sunt inventore in
              est ut optio sequi unde.</p>
          </div>
          <div className="col-lg-3">
            <h3>Where</h3>
            <p>Downtown Conference Center, New York</p>
          </div>
          <div className="col-lg-3">
            <h3>When</h3>
            <p>Monday to Wednesday 10-12 December</p>
          </div>
        </div>
      </div>
    </section>

   
  <section id="schedule">
  <div className="container">
    <div className="section-header">
      <h2>Event Lineups</h2>
      <p>Here is our event schedule</p>
    </div>

    <ul className="nav nav-tabs" role="tablist">
      <li className="nav-item">
        <a className="nav-link active" href="#day-1" role="tab" data-toggle="tab">Day 1</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#day-2" role="tab" data-toggle="tab">Day 2</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#day-3" role="tab" data-toggle="tab">Day 3</a>
      </li>
    </ul>

    <h3 className="sub-heading">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius
      necessitatibus voluptatem quis labore perspiciatis quia.</h3>

    <div className="tab-content row justify-content-center">

     
      <div role="tabpanel" className="col-lg-9 tab-pane fade show active" id="day-1">

        <div className="row schedule-item">
          <div className="col-md-2"><time>09:30 AM</time></div>
          <div className="col-md-10">
            <h4>Registration</h4>
            <p>Fugit voluptas iusto maiores temporibus autem numquam magnam.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>10:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/1.jpg" alt="Brenden Legros" />
            </div>
            <h4>Keynote <span>Brenden Legros</span></h4>
            <p>Facere provident incidunt quos voluptas.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>11:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe" />
            </div>
            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>12:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich" />
            </div>
            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>02:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen" />
            </div>
            <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
            <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>03:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel" />
            </div>
            <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
            <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>04:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow" />
            </div>
            <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
            <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
          </div>
        </div>

      </div>
     
      <div role="tabpanel" className="col-lg-9  tab-pane fade" id="day-2">

        <div className="row schedule-item">
          <div className="col-md-2"><time>10:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/1.jpg" alt="Brenden Legros" />
            </div>
            <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
            <p>Facere provident incidunt quos voluptas.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>11:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe" />
            </div>
            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>12:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich" />
            </div>
            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>02:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen" />
            </div>
            <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
            <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>03:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel" />
            </div>
            <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
            <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>04:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow" />
            </div>
            <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
            <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
          </div>
        </div>

      </div>
    
      <div role="tabpanel" className="col-lg-9  tab-pane fade" id="day-3">

        <div className="row schedule-item">
          <div className="col-md-2"><time>10:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe" />
            </div>
            <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
            <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>11:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich" />
            </div>
            <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
            <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>12:00 AM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/1.jpg" alt="Brenden Legros" />
            </div>
            <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
            <p>Facere provident incidunt quos voluptas.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>02:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen" />
            </div>
            <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
            <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>03:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel" />
            </div>
            <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
            <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
          </div>
        </div>

        <div className="row schedule-item">
          <div className="col-md-2"><time>04:00 PM</time></div>
          <div className="col-md-10">
            <div className="speaker">
              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow" />
            </div>
            <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
            <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
          </div>
        </div>

      </div>
     

    </div>

  </div>

</section>
<section id="buy-tickets" className="section-with-bg">
  <div className="container">

    <div className="section-header">
      <h2>Buy Tickets</h2>
      <p>Velit consequatur consequatur inventore iste fugit unde omnis eum aut.</p>
    </div>

    <div className="row">
      <div className="col-lg-4">
        <div className="card mb-5 mb-lg-0">
          <div className="card-body">
            <h5 className="card-title text-muted text-uppercase text-center">Silver</h5>
            <h6 className="card-price text-center">$150</h6>
            <hr />
            <ul className="fa-ul">
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Regular Seating</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Coffee Break</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Custom Badge</li>
              <li className="text-muted"><span className="fa-li"><i className="fa fa-times"></i></span>Community Access</li>
              <li className="text-muted"><span className="fa-li"><i className="fa fa-times"></i></span>Workshop Access</li>
              <li className="text-muted"><span className="fa-li"><i className="fa fa-times"></i></span>After Party</li>
            </ul>
            <hr />
            <div className="text-center">
              <button type="button" className="btn" data-toggle="modal" data-target="#buy-ticket-modal" data-ticket-type="standard-access">Buy Now</button>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-4">
        <div className="card mb-5 mb-lg-0">
          <div className="card-body">
            <h5 className="card-title text-muted text-uppercase text-center">Gold</h5>
            <h6 className="card-price text-center">$250</h6>
            <hr />
            <ul className="fa-ul">
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Regular Seating</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Coffee Break</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Custom Badge</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Community Access</li>
              <li className="text-muted"><span className="fa-li"><i className="fa fa-times"></i></span>Workshop Access</li>
              <li className="text-muted"><span className="fa-li"><i className="fa fa-times"></i></span>After Party</li>
            </ul>
            <hr />
            <div className="text-center">
              <button type="button" className="btn" data-toggle="modal" data-target="#buy-ticket-modal" data-ticket-type="pro-access">Buy Now</button>
            </div>
          </div>
        </div>
      </div>
     
      <div className="col-lg-4">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title text-muted text-uppercase text-center">Platinum</h5>
            <h6 className="card-price text-center">$350</h6>
            <hr />
            <ul className="fa-ul">
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Regular Seating</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Coffee Break</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Custom Badge</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Community Access</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>Workshop Access</li>
              <li><span className="fa-li"><i className="fa fa-check"></i></span>After Party</li>
            </ul>
            <hr />
            <div className="text-center">
              <button type="button" className="btn" data-toggle="modal" data-target="#buy-ticket-modal" data-ticket-type="premium-access">Buy Now</button>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>

  <div id="buy-ticket-modal" className="modal fade">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">Buy Tickets</h4>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <form method="POST" action="#">
            <div className="form-group">
              <input type="text" className="form-control" name="your-name" placeholder="Your Name" />
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="your-email" placeholder="Your Email" />
            </div>
            <div className="form-group">
              <select id="ticket-type" name="ticket-type" className="form-control" >
                <option value="">-- Select Your Ticket Type --</option>
                <option value="standard-access">Silver</option>
                <option value="pro-access">Gold</option>
                <option value="premium-access">Platinum</option>
              </select>
            </div>
            <div className="text-center">
              <button type="submit" className="btn">Buy Now</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</section>
</main>
 <Footer />
        </div>
  );

};
export default Home;
