import React, { useState,useContext } from "react";
import { Button, Col, Input, message, Row } from "antd";
import "./login.css";
import md5 from "md5"
import {setToken,checkUserLogin} from "./../../Utils/Services/Common"
import { GlobalState } from "./../../../context-api";
import Cookies from 'universal-cookie';

const Login = () => {
    const cookies = new Cookies();

    const { setAuthenticated } = useContext(GlobalState);

  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const SubmitLogin =()=>{
      
   // const token = localStorage.getItem('token');
  //  if(token == '' || token == undefined){
      let data  = {
        email : 'anoopramachandran04@gmail.com',
        password : 'admin@123'
    }
      setToken(data).then((token_response) => {
        console.log("returned access token");
        console.log(token_response.data.access_token);
       
        let payload  = {
          username : Username,
          password : md5(Password),
          TokenValue : token_response.data.access_token
          }
         
         checkUserLogin(payload).then((response) => {
          console.log("returned response");
        console.log(response.data[0].username);
            if (response.data && response.data.length>0) {
              cookies.set('session', true);
              localStorage.setItem('LoggedUserEmail', response.data[0].username);
              setTimeout(() => {
                setAuthenticated(true)
             //   setTotalLoading(false)
              }, 1000);
            }else{
                message.error("Bad credentials, please login again")
             //   setTotalLoading(false)
            }
          });
      });

   // }
 
   
  }

  return (
    <div className="login-page">
      <div className="container-fluid">
        <Row>
          <Col md={24}>
            <div className="login-container">
              <div className="logo-container">
                
              </div>
              <div className="form-container">
                <Input
                  placeholder="username"
                  className="mb-3"
                  value={Username}
                  onChange={(e) => setUsername(e.target.value)}
                />
                <Input
                  placeholder="password"
                  type="password"
                  value={Password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <div className="button-container">
                <Button onClick={()=>SubmitLogin()}>Login</Button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default Login;
