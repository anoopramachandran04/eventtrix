import axiosApiInstance from "./ApiInstance";

async function sendApiRequest(url, formData, reqType) {
  let response;
  if (reqType === "POST") {
    response = await axiosApiInstance
      .post(url, formData)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
    let res = await response;
    if (res) return res.data;
  } else {
    response = await axiosApiInstance
      .get(url)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });
  }
  let res = await response;
  if (res) return res.data;
}

export async function GetData(url) {
  return sendApiRequest(url, {}, "GET");
}
export async function PostData(url, payload) {
  return sendApiRequest(url, payload, "POST");
}

