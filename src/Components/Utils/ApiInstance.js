import axios from "axios";

const axiosApiInstance = axios.create({
  baseURL: "http://127.0.0.1:8000/",
});

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
  async config => {

    const Token = "abc"

    config.headers = {       
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    return config;
  },
  error => {
    Promise.reject(error)
});

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use((response) => {
  return response
}, async function (error) {
  const originalRequest = error.config;
  console.log("access_token");       
    console.log(originalRequest);    
  if (error.response.status === 403 && !originalRequest._retry) {
    originalRequest._retry = true;
    const access_token = localStorage.getItem('token');   
    console.log("access_token");       
    console.log(access_token);         
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
    return axiosApiInstance(originalRequest);
  }
  return Promise.reject(error);
});


export default axiosApiInstance;