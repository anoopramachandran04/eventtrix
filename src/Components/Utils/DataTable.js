import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import cellEditFactory from "react-bootstrap-table2-editor";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { LeftOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";


const { SearchBar } = Search;


const DataTable = ({orderEdit,columns,data,keyField,defaultSorted,UpdateOrderNumber,heading,subheading,goback,gobackurl}) => {
  const customTotal = (from, to, size) => (
    <span className="react-bootstrap-table-pagination-total">
      Showing {from} to {to} of {size} Results
    </span>
  );

  let history = useHistory();

  const options = {
    paginationTotalRenderer: customTotal,
    sizePerPage: 20,
    showTotal: true,
  };

  const ChangeOrderNumber = (oldValue, newValue, row, column) => {
    UpdateOrderNumber(oldValue, newValue, row, column);
  };

  return (
    <div className="data-table">
      
      {goback&&<LeftOutlined  size={40} className="go-back-icon" onClick={()=>history.push({pathname:gobackurl})}/>}<h5>{subheading?heading+" "+subheading:heading}</h5>
      <ToolkitProvider
        
        bootstrap4={true}
            columns={columns}
            data={data}
            keyField={keyField}
        hh={true}
        search
      >
        {(props) => <>
            <div className="table-search-bar"><SearchBar {...props.searchProps} /></div>
            <BootstrapTable
            {...props.baseProps}
            bordered={false}
            responsive={true}
            pagination={paginationFactory(options)}
            defaultSorted={defaultSorted}
            cellEdit={
              orderEdit &&
              cellEditFactory({
                mode: "click",
                afterSaveCell: (oldValue, newValue, row, column) => {
                  ChangeOrderNumber(oldValue, newValue, row, column);
                },
              })
            }
            />
            </>
        }
      </ToolkitProvider>
    </div>
  );
};
export default DataTable;
