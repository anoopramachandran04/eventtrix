import React, { createContext, useState, useEffect } from "react";
import Cookies from 'universal-cookie';


export const GlobalState = createContext();

const cookies = new Cookies();

export const GlobalStateProvider = (props) => {
  const [Authenticated, setAuthenticated] = useState(false);
  const [LoggedUserEmail, setLoggedUserEmail] = useState("");
const [TotalLoading, setTotalLoading] = useState(false)


  useEffect(() => {
    if(cookies.get('session')){
      setAuthenticated(true)
      setLoggedUserEmail(localStorage.getItem('LoggedUserEmail'))
    }else{
      setAuthenticated(false)
    }
  }, [Authenticated])





  return (
    <GlobalState.Provider
      value={{
        Authenticated,
        setAuthenticated,
        LoggedUserEmail,
        setLoggedUserEmail,
        TotalLoading,
        setTotalLoading
      }}
    >
      {props.children}
    </GlobalState.Provider>
  );
};
