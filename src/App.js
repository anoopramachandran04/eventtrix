import React from "react";
import Routes from "./Routes";
import { GlobalStateProvider } from "./context-api";

const App = () => {
  return (
    <>
      <GlobalStateProvider>
       
        <Routes />
      </GlobalStateProvider>
    </>
  );
};

export default App;
